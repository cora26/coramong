export interface Todo {
  id: number;
  content: string;
}

export interface Meta {
  totalCount: number;
}

export interface Item {
  id: number;
  title: string;
  desc: string;
  image: string
  category: string;
  isActive: boolean;
  tel: string;
  location: string;
  address1: string;
  address2: string;
  keywords: string[];
  stars: number;
}

// export interface ItemDetail {
//   id: number;
//   title: string;
//   desc: string;
//   image: string
//   category: string;
//   isActive: boolean;
//   tel: string;
//   location: string;
//   address1: string;
//   address2: string;
//   keywords: string[];
//   stars: number;
// }


